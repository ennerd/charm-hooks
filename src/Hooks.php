<?php
declare(strict_types=1);

namespace Charm;

/**
 * Class that enables intergration between libraries.
 */
class Hooks
{
    public const MAX_DEPTH = 200;

    protected $listeners = [];
    protected static $depth = 0; // record the depth of recursive hooks, to avoid infinite loops
    protected static $index = 0;

    private static $instance;

    /**
     * Get the global hook instance.
     */
    public static function instance(): self
    {
        if (null === static::$instance) {
            static::$instance = new self();
        }

        return static::$instance;
    }

    public function __debugInfo()
    {
        $result = [
            'listeners' => [],
        ];
        foreach ($this->listeners as $name => $listeners) {
            $result['listeners'][$name] = 1 + ($result['listeners'][$name] ?? 0);
        }

        return $result;
    }

    protected function getListeners(string $name)
    {
        return $this->listeners[$name];
    }

    /**
     * Are any listeners registered?
     */
    public function hasListeners(string $name): bool
    {
        return !empty($this->listeners[$name]);
    }

    /**
     * Dispatch a hook. Returns the return value from each listener in an array.
     *
     * @param mixed ...$args
     */
    public function dispatch(string $name, mixed ...$args): array
    {
        if (static::$depth++ > self::MAX_DEPTH) {
            static::stackOverflow($name);
        }

        if (!isset($this->listeners[$name])) {
            --static::$depth;

            return [];
        }

        $results = [];
        foreach ($this->listeners[$name] as $listener) {
            $results[] = \call_user_func_array($listener['callback'], $args);
        }
        --static::$depth;

        return $results;
    }

    /**
     * Not tested.
     */
    public function filter($name, $value, mixed ...$args)
    {
        if (!isset($this->listeners[$name])) {
            return $value;
        }

        if (static::$depth++ > self::MAX_DEPTH) {
            static::stackOverflow($name);
        }

        $argsFinal = [&$value];
        foreach ($args as &$arg) {
            $argsFinal[] = &$arg;
        }

        foreach ($this->listeners[$name] as $listener) {
            $value = \call_user_func_array($listener['callback'], $argsFinal);
        }

        --static::$depth;

        return $value;
    }

    /**
     * Dispatch a hook and stop after the first handler responds with a truthy result.
     */
    public function dispatchToFirst(string $name, mixed ...$args): mixed
    {
        if (!isset($this->listeners[$name])) {
            return null;
        }

        if (static::$depth++ > self::MAX_DEPTH) {
            static::stackOverflow($name);
        }

        foreach ($this->listeners[$name] as $listener) {
            $result = \call_user_func_array($listener['callback'], $args);

            if ($result || \is_array($result)) {
                --static::$depth;

                return $result;
            }
        }
        --static::$depth;

        return null;
    }

    /**
     * Listen on a specified hook.
     *
     * @param string $name     name or identifier of the hook
     * @param mixed  $callback Callback
     *
     * @return mixed Handle of the listener. Can be used to cancel (unlisten).
     */
    public function listen(string $name, callable $callback, int $weight = 0): string
    {
        if (!isset($this->listeners[$name])) {
            $this->listeners[$name] = [];
        }

        $this->listeners[$name][static::$index] = ['callback' => $callback, 'weight' => $weight];

        usort($this->listeners[$name], function ($a, $b) {
            return $a['weight'] < $b['weight'] ? -1 : ($a['weight'] == $b['weight'] ? 0 : 1);
        });

        return $name.':'.static::$index++;
    }

    /**
     * Stop listening for a particular hook.
     */
    public function unlisten(string $handle): void
    {
        list($name, $index) = explode(':', $handle);
        unset($this->listeners[$name][$index]);
    }

    /**
     * Emit an error message related to a particular hook.
     *
     * @param [type] $hookName
     */
    protected static function stackOverflow(string $hookName): void
    {
        throw new Error('Fubber\Hooks stack overflow. Probably recursive hook (name='.$hookName.').');
    }
}
