Charm/Hooks
===========

Provides a set of functions that enables integration between tools.

Usage
-----

Get first truthy response:

```
// Integrating:
Charm\Hooks::instance()->listen('e-mail invalid', function($value) {
    if (str_ends_with($value, '@company.com')) {
        return "E-mail address must end with $company.com";
    }
});

// Enabling integration:
if ($reason = Charm\Hooks::instance()->dispatchToFirst('e-mail invalid')) {
    throw new Exception("Invalid e-mail: $reason");
}
```

Filter some data:

```
// Integrating
Charm\Hooks::instance()->listen('user api-response', function($value) {
    unset($value['email']);
    unset($value['password']);
    return $value;
});

// Filtering
echo json_encode(Charm\Hooks::instance()->filter('user api-response', $user->jsonSerialize()));
```

Query for data from zero or more listeners

```
Charm\Hooks::instance()->listen('toolbar buttons', function() {
    return new Button("Logout");
});

foreach (Charm\Hooks::instance()->dispatch('toolbar buttons') as $button) {
    echo $button->renderHtml();
}
```
